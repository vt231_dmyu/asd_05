#ifndef _CLIST_H_
#define _CLIST_H_

#include <stdlib.h>
#include <stdbool.h>

typedef int elemtype;       // ��� �������� ������

struct elem {
    elemtype* value;        // �������� �����
    struct elem* next;      // ��������� �� ��������� ������� ������
    struct elem* prev;      // ��������� �� ���������� ������� ������
};

struct myList {
    struct elem* head;      // ������ ������� ������
    struct elem* tail;      // �������� ������� ������
    size_t size;            // ʳ������ �������� � ������
};

typedef struct elem cNode;
typedef struct myList cList;

cList* createList(void);
void deleteList(cList** list);
bool isEmptyList(cList* list);
int pushBack(cList* list, elemtype* data);
int popBack(cList* list, elemtype* data);

#endif  //_CLIST_H_