#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>
#include "clist.c"

void swapValues(int* xp, int* yp) {
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void swapValuesDll(struct elem* xp, struct elem* yp) {
	elemtype* temp = xp->value;
	xp->value = yp->value;
	yp->value = temp;
}

void dllSelectionSort(struct elem** head) {
    struct elem* i = *head;

    while (i != NULL) {
        struct elem* min = i;
        struct elem* j = i->next;

        // ����������� ����� �����'������ ������, ��� ������ ���������� �������
        while (j != NULL) {
            if (*(j->value) < *(min->value)) // ��������� �������� ��������
                min = j;
            j = j->next;
        }

        // ̳����� ������ ���� ��������� ����� �� ����������� ���������� �����
        swapValuesDll(i, min);

        i = i->next;
    }
}

void insertionSort(int arr[], int n) {
	int i, j, key;
	for (i = 1; i < n; i++) {
		key = arr[i];
		j = i - 1;

		// ��������� ��������, �� � ������� �� key, �� ���� ������� �������� �� ������� �������
		while (j >= 0 && arr[j] > key) {
			arr[j + 1] = arr[j];
			j = j - 1;
		}
		arr[j + 1] = key;
	}
}

void dllInsertionSort(struct elem** head) {
	struct elem* i, * j, * key;
	for (i = (*head)->next; i != NULL; i = i->next) {
		key = i;
		j = i->prev;

		// ��������� ��������, �� � ������� �� key, �� ���� ������� �������� �� ������� �������
		while (j != NULL && *(j->value) > *(key->value)) {
			swapValuesDll(j, key); // ���� ������� �������� ������
			key = key->prev; // ���������� �� ������������ ��������
			j = j->prev;
		}
	}
}


void generateRandomData(int arr[], int size) {
	for (int i = 0; i < size; i++) {
		arr[i] = rand() % 1000; // �������� ����� �� 0 �� 999
		//printf("%d ", arr[i]);
	}
	//printf("\n");
}

void dllGenerateRandomData(cList* list, int size) {
	for (int i = 0; i < size; i++) {
		elemtype data = rand() % 1000;
		pushBack(list, &data); // �������� ����� �� 0 �� 999
	}
	//struct elem* current = list->head; // �������� � ������ ������
	//while (current != NULL) {
	//	printf("%d ", *(current->value)); // �������� �������� ��������� ��������
	//	current = current->next; // ���������� �� ���������� ��������
	//}
	//printf("\n");
}

int main() {
	system("chcp 1251"); // ������� � ������ �� ��������
	system("cls"); // ������� ���� ������
	srand(time(NULL));

	int s;
	int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };
	//int sizes[] = { 10, 100 };

	for (int i = 0; i < sizeof(sizes) / sizeof(sizes[0]); i++) {
		cList* mylist = createList();
		s = sizes[i];
		dllGenerateRandomData(mylist, s);

		auto begin = GETTIME();
		dllSelectionSort(&(mylist->head));
		auto end = GETTIME();
		auto spent = CALCTIME(end - begin);

		//printf("\n---\n");
		//struct elem* current = mylist->head; // �������� � ������ ������
		//while (current != NULL) {
		//	printf("%d ", *(current->value)); // �������� �������� ��������� ��������
		//	current = current->next; // ���������� �� ���������� ��������
		//}
		//printf("\n---\n");
		//printf("\n");

		printf("��� ���������� �� ���������� ������� �����'������ ������ � %d ��������: %lld ��.\n", s, spent.count());
		deleteList(&mylist);
	}
	printf("\n");
	for (int i = 0; i < sizeof(sizes) / sizeof(sizes[0]); i++) {
		s = sizes[i];
		int* arr = (int*)malloc(sizeof(int) * s);
		generateRandomData(arr, s);

		auto begin = GETTIME();
		insertionSort(arr, s);
		auto end = GETTIME();
		auto spent = CALCTIME(end - begin);

		//printf("\n---\n");
		//for (int i = 0; i < s; i++) {
		//	printf("%d ", arr[i]);
		//}
		//printf("\n---\n");
		//printf("\n");

		printf("��� ���������� �� ���������� ��������� ������ � %d ��������: %lld ��.\n", s, spent.count());
		free(arr);
	}
	printf("\n");
	for (int i = 0; i < sizeof(sizes) / sizeof(sizes[0]); i++) {
		cList* mylist = createList();
		s = sizes[i];
		dllGenerateRandomData(mylist, s);

		auto begin = GETTIME();
		dllInsertionSort(&(mylist->head));
		auto end = GETTIME();
		auto spent = CALCTIME(end - begin);

		//printf("\n---\n");
		//struct elem* current = mylist->head; // �������� � ������ ������
		//while (current != NULL) {
		//	printf("%d ", *(current->value)); // �������� �������� ��������� ��������
		//	current = current->next; // ���������� �� ���������� ��������
		//}
		//printf("\n---\n");
		//printf("\n");

		printf("��� ���������� �� ���������� ��������� �����'������ ������ � %d ��������: %lld ��.\n", s, spent.count());
		deleteList(&mylist);
	}

	return 0;
}