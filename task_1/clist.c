#include "clist.h"

// ��������� ���������� ������
cList* createList(void) {
    cList* list = (cList*)malloc(sizeof(cList));
    if (list) {
        list->size = 0;
        list->head = list->tail = NULL;
    }
    return list;
}

// ��������� ������
void deleteList(cList** list) {
    cNode* head = (*list)->head;
    cNode* next = NULL;
    while (head) {
        next = head->next;
        free(head);
        head = next;
    }
    free(*list);
    *list = NULL;
}

// �������� ������ �� ���������
bool isEmptyList(cList* list) {
    return ((list->head == NULL) || (list->tail == NULL));
}

// ��������� ������ ����� � ����� ������
int pushBack(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return(-3);
    }
    node->value = (elemtype*)malloc(sizeof(elemtype)); // �������� ���'�� ��� ��������
    if (!node->value) {
        free(node);
        return(-3);
    }
    *(node->value) = *data; // ��������� ��������
    node->next = NULL;
    node->prev = list->tail;
    if (!isEmptyList(list)) {
        list->tail->next = node;
    }
    else {
        list->head = node;
    }
    list->tail = node;

    list->size++;
    return(0);
}

// ����� ����� � ���� ������
int popBack(cList* list, elemtype* data) {
    cNode* node = NULL;

    if (isEmptyList(list)) {
        return(-4);
    }

    node = list->tail;
    list->tail = list->tail->prev;
    if (!isEmptyList(list)) {
        list->tail->next = NULL;
    }
    else {
        list->head = NULL;
    }

    data = node->value;
    list->size--;
    free(node);

    return(0);
}
